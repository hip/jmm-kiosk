const socket =      require("socket.io");
const express =     require("express");

// =========== App setup ============
const app = express();
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));

// listen on port 8000 with http server
const server = app.listen(8000, function () {  
    console.log(`JMM Applet: listening on http://localhost:8000`);
});

// Routing
app.get('/', function(req, res) {           // http://localhost:8000
    res.render("index.ejs");
});
app.get('/controller', function(req, res) { // http://localhost:8000/controller
    res.render("controller.ejs");
});
app.get('/display', function(req, res) {    // http://localhost:8000/display
    res.render("display.ejs");
});

// =========== Web Socket setup ===========
const io = socket(server);
io.on("connection", function (socket) { // listen for connecting clients
    console.log("--> A device has connected.");

    // Mark connections as either controller or display
    socket.on("connect_controller", function () {
        console.log("---> Device associated as CONTROLLER.")
    });
    socket.on("connect_display", function () {
        console.log("---> Device associated as DISPLAY.")
    });

    // Communication coming from display, going to controller
    socket.on("from_display", function(data) {
        console.log("----> Passing from display to controller...");
        io.emit("to_controller", data);
    });
    // Communication coming from controller, going to display
    socket.on("from_controller", function(data) {
        console.log("----> Passing from controller to display...");
        io.emit("to_display", data);
    });
});