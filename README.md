# jmm-kiosk

## Setup
1. Install NodeJS and npm: https://docs.npmjs.com/downloading-and-installing-node-js-and-npm
2. Clone this repository, `cd` into it in a terminal, and run `npm install` to install all dependencies.
3. Use `npm start` to run the start script and start the app.
4. Open `localhost:8000` in your browser.
